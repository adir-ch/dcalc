package main

import service "gitlab.com/adir-ch/dcalc/mul/cmd/service"

var (
	Version string
	Build   string
)

func main() {
	service.Run()
}
