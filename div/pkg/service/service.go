package service

import "fmt"

// Service - checkout service interface
type Service interface {
	Div(numbers []float64) (float64, error)
}

// ServiceFunc - extension func
type ServiceFunc func([]float64) (float64, error)

// Div - Service div extension func
func (s ServiceFunc) Div(numbers []float64) (float64, error) {
	return s(numbers)
}

// New - creates a new checkout service + middleware
func New(middleware []ServiceMiddleware) Service {
	var svc Service = NewBasicDivService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

// BasicDivService - the basic div service
type BasicDivService struct{}

// NewBasicDivService - creates a new basic checkout service
func NewBasicDivService() BasicDivService {
	return BasicDivService{}
}

// Div - make the calculation
func (b BasicDivService) Div(numbers []float64) (float64, error) {
	if len(numbers) == 0 {
		return 0, fmt.Errorf("no numbers received")
	}

	rs := numbers[0]
	for i := 1; i < len(numbers); i++ {
		if numbers[i] == 0 {
			return 0, fmt.Errorf("cannot divide by zero")
		}
		rs = rs / numbers[i]
	}
	return rs, nil
}
