package service

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"log"
	"time"
)

// ServiceMiddleware - middleware helper
type ServiceMiddleware func(Service) Service

type totalDivEvent struct {
	msg   string
	total float64
}

// DomainEventMw - Domain event dispatcher
type DomainEventMw struct {
	dispatcher *log.Logger
	next       Service
}

func (d DomainEventMw) dispatchEvent(event totalDivEvent) {
	d.dispatcher.Printf("dispatching event: %+v\n", event)
}

// Div - implement Div interface
func (d DomainEventMw) Div(numbers []float64) (float64, error) {
	total, err := d.next.Div(numbers)
	status := "OK"
	if err != nil {
		status = err.Error()
	}

	e := totalDivEvent{
		msg:   fmt.Sprintf("totalDivEvent - status: %s, numbers: %v, result: %.2f", status, numbers, total),
		total: total,
	}
	d.dispatcher.Printf("event dispatched: %+v\n", e)
	return total, err
}

// GetDomainEventMw - creates a domain event sender
func GetDomainEventMw(logger *log.Logger) ServiceMiddleware {
	return func(s Service) Service {
		return &DomainEventMw{dispatcher: logger, next: s}
	}
}

// GetDomainEventMwFunc - creates a domain event sender
func GetDomainEventMwFunc(dispatcher *log.Logger) ServiceMiddleware {
	return func(s Service) Service {
		f := func(numbers []float64) (float64, error) {
			total, err := s.Div(numbers)
			status := "finished successfully"
			if err != nil {
				status = fmt.Sprintf("unsuccessful - %s", err)
			}
			e := totalDivEvent{
				msg:   fmt.Sprintf("totalDivEvent - status: %s, numbers: %v, result: %.2f", status, numbers, total),
				total: total,
			}
			dispatcher.Printf("event dispatched: %+v\n", e)
			return total, err
		}
		return ServiceFunc(f)
	}
}

// GetProcessTimingMw - creates a domain event sender
func GetProcessTimingMw() ServiceMiddleware {
	return func(s Service) Service {
		f := func(numbers []float64) (float64, error) {
			start := time.Now()
			t, e := s.Div(numbers)
			fmt.Printf("div time took: %s\n", (fmt.Sprint(time.Since(start).Nanoseconds()/1000000) + "ms."))
			return t, e
		}
		return ServiceFunc(f)
	}
}

// GetReqMetricsMw - request metrics middleware
func GetReqMetricsMw(counter prometheus.Counter) ServiceMiddleware {
	return func(s Service) Service {
		f := func(numbers []float64) (float64, error) {
			counter.Inc()
			t, e := s.Div(numbers)
			return t, e
		}
		return ServiceFunc(f)
	}
}
