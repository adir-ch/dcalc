package endpoint

import (
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// Middleware - middleware define
type Middleware func(Endpoint) Endpoint

// EndpointFunc - middleware funcs
type EndpointFunc func(*DivRequest) *DivResponse

// Div - middleware func
func (e EndpointFunc) Div(request *DivRequest) *DivResponse {
	return e(request)
}

// GetEndpointDurationMw - request duration metric middleware
func GetEndpointDurationMw(duration *prometheus.SummaryVec) Middleware {
	return func(e Endpoint) Endpoint {
		f := func(request *DivRequest) *DivResponse {
			begin := time.Now()
			res := e.Div(request)
			duration.WithLabelValues("div", fmt.Sprintf("%t", (res.Err == nil))).Observe(time.Since(begin).Seconds())
			return res
		}

		return EndpointFunc(f)
	}
}
