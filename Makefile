#--- Makefile ----

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOFORMAT=$(GOCMD) fmt

VERSION=$(shell git describe --abbrev=0 --tags)
BUILD=$(shell date +%FT%T%z)
LDFLAGS=-ldflags "-X main.Version=$(VERSION) -X main.Build=$(BUILD)"
STATIC=-ldflags "-linkmode external -extldflags -static"
SERVICES=calc add sub mul div
INFRA=etcd nats jaeger
SRV=$(arg)

all: show-ver lint sec-check
		
show-ver:
	@echo "Building Ver: ${VERSION} Date: ${BUILD} "

build: show-ver
	docker-compose build --build-arg VER=$(VERSION) --build-arg BUILD=$(BUILD) $(SRV)

clean-modules:
	$(GOCMD) clean -modcache 
	$(GOCMD) mod tidy

rebuild-service: 
	docker-compose stop --force $(SRV); \
	docker-compose build --force-rm --no-cache $(SRV); \

clean-service:
	docker-compose stop --force $(SRV); \
	docker-compose rm --force $(SRV); \

build-all-services: 
	for service in $(SERVICES) ; do \
        echo "---- building $$service ----"; \
		docker-compose build $$service; \
    done \

rebuild-all-services: stop-services
	for service in $(SERVICES) ; do \
        echo "---- rebuilding $$service ----"; \
		docker-compose build --force-rm --no-cache $$service; \
    done \

clean-all-services: stop-services
	for service in $(SERVICES) ; do \
        echo "---- claning service: $$service ----"; \
		docker-compose rm --force $$service; \
    done \
	
run: run-infra run-services
stop: stop-services stop-infra

run-services:
	docker-compose up $(SERVICES)

run-infra:
	docker-compose up -d $(INFRA)

stop-services: 
	docker-compose stop $(SERVICES)

stop-infra:
	docker-compose stop $(INFRA)

test: uni-test integration-test

uni-test: 
	$(GOTEST) -v -cover --race ./... 

bench-test:
	$(GOTEST) ./... -bench=. -benchmem # -benchtime=1s

integration-test:
	./test.sh

lint:
	gometalinter ./... > lint.log

list-modules: 
	$(GOCMD) list -m all

status: 
	docker-compose ps

delete-docker-none-images:
	@echo "cleaning docker <none> images"
	@docker rmi $(shell docker images -q -f dangling=true) --force

docker-prune:
	docker system prune