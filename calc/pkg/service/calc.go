package service

import (
	"context"
	"fmt"
	"go/ast"
	"go/parser"
	"reflect"
	"strconv"
	"strings"

	log "github.com/go-kit/kit/log"
	"github.com/opentracing/opentracing-go"
)

func eval(ctx context.Context, tracer opentracing.Tracer, logger log.Logger, expr string) (float64, error) {
	//fs := token.NewFileSet()
	tr, err := parser.ParseExpr(expr)
	if err != nil {
		return 0, err
	}

	//ast.Print(fs, tr)
	result, err := traverseTree(ctx, tracer, logger, tr)
	fmt.Printf("eval: %s = %f\n", expr, result)
	return result, err
}

func traverseTree(ctx context.Context, tracer opentracing.Tracer, logger log.Logger, node ast.Node) (float64, error) {
	//fmt.Printf("node is: %s\n", reflect.TypeOf(node))
	switch n := node.(type) {
	case *ast.ParenExpr:
		return traverseTree(ctx, tracer, logger, n.X)
	case *ast.BinaryExpr:
		X, err := traverseTree(ctx, tracer, logger, n.X)
		if err != nil {
			return 0, err
		}
		Y, err := traverseTree(ctx, tracer, logger, n.Y)
		if err != nil {
			return 0, err
		}

		return callOp(ctx, tracer, logger, n.Op.String(), []float64{X, Y})

	case *ast.UnaryExpr:
		return 0, fmt.Errorf("unary not supported: %v", n)
	case *ast.BasicLit:
		//fmt.Printf("lit: %s\n", n.Value)
		if v, err := strconv.ParseFloat(strings.TrimSpace(n.Value), 64); err != nil {
			return 0, fmt.Errorf("unable to convert number to float64: %s", err)
		} else {
			return v, nil
		}

	default:
		return 0, fmt.Errorf("not supported node: %s", reflect.TypeOf(node))
	}

	return 0, fmt.Errorf("mmm...should not get here :(")
}
