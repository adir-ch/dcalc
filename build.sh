#! /bin/bash

set -e # exit when something fails

echo "--- building code --- "
echo 

target=$1

if [ "$target" == "" ] 
then    
    echo ""
    echo "--- building all services ---" 
    echo ""
else 
    echo ""
    echo "--- building target/s $target ---" 
    echo ""
fi

ls -d */ | xargs -e basename -a | while read service 
    do 
        #echo "check service $service"
        if [ "$target" != "" ] && [ "$target" != "$service" ]
        then 
            continue
        fi

        echo "building service $service ---> start"
        make build SRV=$service
    done