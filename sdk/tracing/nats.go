package tracing

import (
	"bytes"
	"context"
	"fmt"

	kitlog "github.com/go-kit/kit/log"
	log "github.com/go-kit/kit/log"
	kitnats "github.com/go-kit/kit/transport/nats"
	nats "github.com/nats-io/nats.go"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

type NATSTraceMsg struct {
	bytes.Buffer
}

func NewNATSTraceMsg(m *nats.Msg) *NATSTraceMsg {
	b := bytes.NewBuffer(m.Data)
	return &NATSTraceMsg{*b}
}

func ContextToNATS(ctx context.Context, tracer opentracing.Tracer, logger log.Logger, subject string, req []byte) (NATSTraceMsg, opentracing.Span) {
	var t NATSTraceMsg // A NATS OpenTracing Message.

	rootSpan := opentracing.SpanFromContext(ctx)
	if rootSpan == nil {
		return t, nil
	}

	reqSpan := tracer.StartSpan("Calc/Mul/req", ext.SpanKindRPCClient, opentracing.ChildOf(rootSpan.Context()))
	ext.MessageBusDestination.Set(reqSpan, subject)

	// Log our request
	reqSpan.LogEvent("Starting request.")

	// Inject the span context into the TraceMsg.
	if err := tracer.Inject(reqSpan.Context(), opentracing.Binary, &t); err != nil {
		logger.Log("NATS", fmt.Sprintf("error in NATS req span inject: %s", err.Error()))
		return t, nil
	}

	// Add the payload.
	t.Write(req)
	logger.Log("NATS", fmt.Sprintf("req: %s", t.String()))
	return t, reqSpan
}

func NATSToContext(tracer opentracing.Tracer, operationName string, logger log.Logger) kitnats.RequestFunc {
	return func(ctx context.Context, msg *nats.Msg) context.Context {
		// Create new TraceMsg from the NATS message.
		t := NewNATSTraceMsg(msg)
		logger.Log("NATS", fmt.Sprintf("req: %s", t.String()))

		// Extract the span context from the request message.
		spanContext, err := tracer.Extract(opentracing.Binary, t)
		if err != nil {
			logger.Log("NATS", fmt.Sprintf("cannot find span in request: %s", err.Error()))
			return ctx // return the context with no change
		}

		// Setup a span referring to the span context of the incoming NATS message.
		span := tracer.StartSpan(operationName, ext.SpanKindRPCServer, ext.RPCServerOption(spanContext))
		msg.Data = t.Bytes()
		return opentracing.ContextWithSpan(ctx, span)
	}
}

func NATSSpanFinish(name string, tracer opentracing.Tracer, logger kitlog.Logger) kitnats.SubscriberResponseFunc {
	return func(ctx context.Context, conn *nats.Conn) context.Context {
		span := opentracing.SpanFromContext(ctx)
		logger.Log(name, "span finalized")
		span.Finish()
		return ctx
	}
}
