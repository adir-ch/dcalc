package tracing

import (
	"fmt"
	"io"

	log "github.com/go-kit/kit/log"
	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
)

// InitJaegerCollector - init a new Jeager collector
func InitJaegerCollector(serviceName string, logger log.Logger) (opentracing.Tracer, io.Closer) {
	cfg, err := config.FromEnv()
	if err != nil {
		// parsing errors might happen here, such as when we get a string where we expect a number
		logger.Log("calc", "tracing", "err", fmt.Sprintf("Could not parse Jaeger env vars: %s", err.Error()))
		fmt.Printf("ERROR: cannot init Jaeger: %v\n", err)
	}

	if serviceName == "" {
		serviceName = "none"
	}
	tracer, closer, err := cfg.New(serviceName, config.Logger(jaeger.StdLogger))
	if err != nil {
		fmt.Printf("ERROR: cannot init Jaeger: %v\n", err)
	}
	return tracer, closer
}
