package tracing

import (
	"context"
	"fmt"

	kitlog "github.com/go-kit/kit/log"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	opentracing "github.com/opentracing/opentracing-go"
	"google.golang.org/grpc/metadata"
)

func GRPCSpanFinishRes(name string, tracer opentracing.Tracer, logger kitlog.Logger) kitgrpc.ServerResponseFunc {
	return func(ctx context.Context, header *metadata.MD, trailer *metadata.MD) context.Context {
		span := opentracing.SpanFromContext(ctx)
		logger.Log(name, "span finished", "data", fmt.Sprintf("span: %v", span))
		span.Finish()
		return ctx
	}
}
