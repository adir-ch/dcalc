package discovery

import (
	"context"
	"fmt"

	log "github.com/go-kit/kit/log"
	sdetcd "github.com/go-kit/kit/sd/etcd"
)

var registry = "http://etcd:2379" // TODO: take from config

func EtcdRegister(prefix, instance string, logger log.Logger) (*sdetcd.Registrar, error) {
	etcdServer := "http://etcd:2379"
	key := fmt.Sprintf("/services/%s/%s", prefix, instance)

	client, err := sdetcd.NewClient(context.Background(), []string{etcdServer}, sdetcd.ClientOptions{})
	if err != nil {
		return nil, err
	}

	registrar := sdetcd.NewRegistrar(client, sdetcd.Service{
		Key:   key,
		Value: instance,
	}, logger)

	registrar.Register()
	logger.Log("register", fmt.Sprintf("service %s is registered in etcd", instance))
	return registrar, nil
}

func EtcdDeregister(service string, r *sdetcd.Registrar, logger log.Logger) {
	logger.Log("deregistering", service)
	r.Deregister()
}

func EtcdDiscover(name string) (string, error) {
	client, err := sdetcd.NewClient(context.Background(), []string{registry}, sdetcd.ClientOptions{})
	if err != nil {
		return "", fmt.Errorf("unable to connect to registry: %s", err.Error())
	}

	entries, err := client.GetEntries(name)
	if err != nil {
		return "", fmt.Errorf("unable to discover %s, err: %s", name, err.Error())
	}

	if len(entries) == 0 {
		return "", fmt.Errorf("unable to discover, err: no entries found")
	}

	return entries[0], nil // TODO: implement some round robin or something similar
}
