package main

import service "gitlab.com/adir-ch/dcalc/sub/cmd/service"

var (
	Version string
	Build   string
)

func main() {
	service.Run()
}
